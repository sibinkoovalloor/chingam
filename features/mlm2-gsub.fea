# Copyright 2019-2021 Rajeesh KV <rajeeshknambiar@gmail.com>, Hussain KH <hussain.rachana@gmail.com>,
## Rachana Institute of Typography <rachana.org.in>
# This file is licensed under OFL 1.1

# Malayalam OpenType feature program for comprehensive orthography in traditional script
# Version 2.0

languagesystem DFLT dflt;
languagesystem MAL dflt;
languagesystem mlm2 dflt;
languagesystem latn dflt;

#----------
#Lookups
#----------

lookup akhn_cil {
    sub nh xx ZWJ  by nhcil;
    sub n1 xx ZWJ  by n1cil;
    sub r3 xx ZWJ  by r3cil;
    sub l3 xx ZWJ  by l3cil;
    sub lh xx ZWJ  by lhcil;
} akhn_cil;

lookup akhn_ra_signs{
    sub n1 xx th1 xx r3 u1   by n1th1r3u1;
    sub n1 xx th1 xx r3 u2   by n1th1r3u2;
    sub n1 xx th3 xx r3 u1   by n1th3r3u1;
    sub n1 xx th3 xx r3 u2   by n1th3r3u2;
    sub m1 xx p1 xx r3 u1   by m1p1r3u1;
    sub m1 xx p1 xx r3 u2   by m1p1r3u2;
    sub k1 xx r3 u1   by k1r3u1;
    sub k1 xx r3 u2   by k1r3u2;
    sub k3 xx r3 u1   by k3r3u1;
    sub k3 xx r3 u2   by k3r3u2;
    sub t1 xx r3 u1   by t1r3u1;
    sub t1 xx r3 u2   by t1r3u2;
    sub t3 xx r3 u1   by t3r3u1;
    sub t3 xx r3 u2   by t3r3u2;
    sub th1 xx r3 u1   by th1r3u1;
    sub th1 xx r3 u2   by th1r3u2;
    sub th3 xx r3 u1   by th3r3u1;
    sub th3 xx r3 u2   by th3r3u2;
    sub th4 xx r3 u1   by th4r3u1;
    sub th4 xx r3 u2   by th4r3u2;
    sub p1 xx r3 u1   by p1r3u1;
    sub p1 xx r3 u2   by p1r3u2;
    sub p2 xx r3 u1   by p2r3u1;
    sub p2 xx r3 u2   by p2r3u2;
    sub p3 xx r3 u1   by p3r3u1;
    sub p3 xx r3 u2   by p3r3u2;
    sub p4 xx r3 u1   by p4r3u1;
    sub p4 xx r3 u2   by p4r3u2;
    sub z1 xx r3 u1   by z1r3u1;
    sub z1 xx r3 u2   by z1r3u2;
    sub s1 xx r3 u1   by s1r3u1;
    sub s1 xx r3 u2   by s1r3u2;
    sub h1 xx r3 u1   by h1r3u1;
    sub h1 xx r3 u2   by h1r3u2;
    sub r3 u1    by r3u1;
    sub r3 u2    by r3u2;
} akhn_ra_signs;

lookup akhn_la_signs{
    sub k1 xx l3 u1   by k1l3u1;
    sub k1 xx l3 u2   by k1l3u2;
    sub k3 xx l3 u1   by k3l3u1;
    sub k3 xx l3 u2   by k3l3u2;
    sub th1 xx l3 u1  by th1l3u1;
    sub p1 xx l3 u1   by p1l3u1;
    sub p1 xx l3 u2   by p1l3u2;
    sub p2 xx l3 u1   by p2l3u1;
    sub p2 xx l3 u2   by p2l3u2;
    sub p3 xx l3 u1   by p3l3u1;
    sub p3 xx l3 u2   by p3l3u2;
    sub l3 xx l3 u1   by l3l3u1;
    sub l3 xx l3 u2   by l3l3u2;
    sub s1 xx l3 u1   by s1l3u1;
    sub s1 xx l3 u2   by s1l3u2;
    sub l3 u1    by l3u1;
    sub l3 u2    by l3u2;
} akhn_la_signs;

lookup akhn_ra_nosigns{
    sub k1 xx k1 xx r3      by k1k1r3;
    sub k1 xx t1 xx r3      by k1t1r3;
    sub k1 xx th1 xx r3     by k1th1r3;
    sub th1 xx th1 xx r3      by th1th1r3;
    sub n1 xx th1 xx r3 by n1th1r3;
    sub n1 xx th3 xx r3 by n1th3r3;
    sub n1 xx th4 xx r3 by n1th4r3;
    sub m1 xx p1 xx r3 by m1p1r3;
#    sub sh xx k1 xx r3    by shk1r3;
#    sub sh xx p1 xx r3    by shp1r3;
#    sub sh xx t1 xx r3    by sht1r3;
#    sub s1 xx k1 xx r3    by s1k1r3;
#    sub s1 xx t1 xx r3    by s1t1r3;
#    sub s1 xx th1 xx r3    by s1th1r3;
#    sub s1 xx p1 xx r3    by s1p1r3;
    sub k1 xx r3    by k1r3;
    sub k3 xx r3    by k3r3;
    sub k4 xx r3    by k4r3;
    sub ch2 xx r3       by ch2r3;
    sub ch3 xx r3    by ch3r3;
    sub t1 xx r3    by t1r3;
    sub t3 xx r3    by t3r3;
    sub t4 xx r3    by t4r3;
    sub th1 xx r3    by th1r3;
    sub th3 xx r3    by th3r3;
    sub th4 xx r3    by th4r3;
    sub p1 xx r3    by p1r3;
    sub p2 xx r3    by p2r3;
    sub p3 xx r3    by p3r3;
    sub p4 xx r3    by p4r3;
    sub m1 xx r3    by m1r3;
    sub v1 xx r3    by v1r3;
    sub z1 xx r3    by z1r3;
    sub sh xx r3    by shr3;
    sub s1 xx r3    by s1r3;
    sub h1 xx r3    by h1r3;
} akhn_ra_nosigns;

lookup akhn_la_nosigns{
    sub l3 xx l3    by l3l3;
    sub k1 xx l3    by k1l3;
    sub k3 xx l3    by k3l3;
    sub th1 xx l3    by th1l3;
    sub p1 xx l3    by p1l3;
    sub p2 xx l3    by p2l3;
    sub p3 xx l3    by p3l3;
    sub m1 xx l3    by m1l3;
    sub v1 xx l3    by v1l3;
    sub z1 xx l3    by z1l3;
    sub s1 xx l3    by s1l3;
    sub h1 xx l3    by h1l3;
} akhn_la_nosigns;

lookup akhn_signs {
#    sub y1 xx k1 xx k1 u1   by y1k1k1u1;
#    sub y1 xx k1 xx k1 u2   by y1k1k1u2;
    sub s1 xx rh xx rh u1   by s1rhrhu1;
    sub s1 xx rh xx rh u2   by s1rhrhu2;
    # 2 chars

    sub k1 xx k1 u1   by k1k1u1;
    sub k1 xx k1 u2   by k1k1u2;
    sub k1 xx k1 r1   by k1k1r1;
    sub k1 xx th1 r1   by k1th1r1;
    sub k1 xx sh u1   by k1shu1;
    sub k1 xx sh u2   by k1shu2;
    sub k3 xx k3 u2   by k3k3u2;
    sub k3 xx n1 u1   by k3n1u1;
    sub ng xx k1 u1   by ngk1u1;
    sub ng xx k1 u2   by ngk1u2;
    sub ng xx k1 r1   by ngk1r1;

    sub ng xx ng u1   by ngngu1;
    sub ng xx ng u2   by ngngu2;

    sub ch1 xx ch1 u1   by ch1ch1u1;
    sub ch1 xx ch1 u2   by ch1ch1u2;
    sub ch3 xx ch3 u1   by ch3ch3u1;
    sub ch3 xx ch3 u2   by ch3ch3u2;
    sub ch3 xx ch3 r1   by ch3ch3r1;
    sub nj xx ch1 u1   by njch1u1;
    sub nj xx ch1 u2   by njch1u2;
    sub nj xx ch3 u1   by njch3u1;
    sub nj xx ch3 u2   by njch3u2;

    sub nj xx nj u1   by njnju1;
    sub nj xx nj u2   by njnju2;

    sub t1 xx t1 u1   by t1t1u1;
    sub t1 xx t1 u2   by t1t1u2;

    sub t3 xx t3 u1   by t3t3u1;
    sub nh xx t1 u1   by nht1u1;
    sub nh xx t1 u2   by nht1u2;

    sub nh xx t3 u1   by nht3u1;
    sub nh xx t3 u2   by nht3u2;
    sub nh xx nh u1   by nhnhu1;
    sub nh xx nh u2   by nhnhu2;

    sub nh xx m1 u1   by nhm1u1;
    sub nh xx m1 u2   by nhm1u2;

    sub th1 xx th1 u1   by th1th1u1;
    sub th1 xx th1 u2   by th1th1u2;
    sub th1 xx th1 r1   by th1th1r1;
    sub th1 xx th2 u1   by th1th2u1;
    sub th1 xx th2 u2   by th1th2u2;

    sub th1 xx p4 u1   by th1p4u1;
    sub th1 xx p4 u2   by th1p4u2;

    sub th1 xx m1 u1   by th1m1u1;
    sub th1 xx m1 u2   by th1m1u2;

    sub th1 xx s1 u1   by th1s1u1;
    sub th1 xx s1 u2   by th1s1u2;
    sub th3 xx th3 u1   by th3th3u1;
    sub th3 xx th3 u2   by th3th3u2;

    sub th3 xx th4 u1   by th3th4u1;
    sub th3 xx th4 u2   by th3th4u2;
    sub n1 xx th1 u1   by n1th1u1;
    sub n1 xx th1 u2   by n1th1u2;

    sub n1 xx th2 u1   by n1th2u1;
    sub n1 xx th2 u2   by n1th2u2;

    sub n1 xx th3 u1   by n1th3u1;
    sub n1 xx th3 u2   by n1th3u2;

    sub n1 xx th4 u1   by n1th4u1;
    sub n1 xx th4 u2   by n1th4u2;

    sub n1 xx n1 u1   by n1n1u1;
    sub n1 xx n1 u2   by n1n1u2;

    sub n1 xx m1 u1   by n1m1u1;
    sub n1 xx m1 u2   by n1m1u2;

    sub [n1 n1cil] xx rh u1   by n1rhu1;
    sub [n1 n1cil] xx rh u2   by n1rhu2;
    sub p1 xx p1 u1   by p1p1u1;
    sub p1 xx p1 u2   by p1p1u2;
    sub p3 xx th3 u1   by p3th3u1;
    sub p3 xx th3 u2   by p3th3u2;
    sub p3 xx p3 u1   by p3p3u1;
    sub p3 xx p3 u2   by p3p3u2;
    sub m1 xx p1 u1   by m1p1u1;
    sub m1 xx p1 u2   by m1p1u2;

    sub m1 xx m1 u1   by m1m1u1;
    sub m1 xx m1 u2   by m1m1u2;
#    sub y1 xx th1 u1   by y1th1u1;
#    sub y1 xx th1 u2   by y1th1u2;
    sub y1 xx y1 u1   by y1y1u1;
    sub y1 xx y1 u2   by y1y1u2;
    sub v1 xx v1 u1   by v1v1u1;
    sub v1 xx v1 u2   by v1v1u2;
#    sub z1 xx z1 u1   by z1z1u1;
#    sub z1 xx z1 u2   by z1z1u2;

#    sub sh xx k1 r1   by shk1r1;
#    sub sh xx t2 u2   by sht2u2;

#    sub sh xx nh u1   by shnhu1;
    sub s1 xx k1 u1   by s1k1u1;
#    sub s1 xx k1 u2   by s1k1u2;
#    sub s1 xx k1 r1   by s1k1r1;
#    sub s1 xx th1 u1   by s1th1u1;
#    sub s1 xx th1 u2   by s1th1u2;
#    sub s1 xx th1 r1   by s1th1r1;

    sub s1 xx th2 u1   by s1th2u1;
    sub s1 xx th2 u2   by s1th2u2;
#    sub s1 xx p1 u1   by s1p1u1;
#    sub s1 xx p1 u2   by s1p1u2;
#    sub s1 xx p1 r1   by s1p1r1;

#    sub s1 xx p2 u1   by s1p2u1;

#    sub s1 xx m1 u1   by s1m1u1;
#    sub s1 xx m1 u2   by s1m1u2;
#    sub s1 xx m1 r1   by s1m1r1;

    sub s1 xx s1 u1   by s1s1u1;
    sub s1 xx s1 u2   by s1s1u2;
    sub lh xx lh u1   by lhlhu1;
    sub lh xx lh u2   by lhlhu2;
    sub rh xx rh u1   by rhrhu1;
    sub rh xx rh u2   by rhrhu2;

    # Single chars

    sub k1 u1    by k1u1;
    sub k1 u2    by k1u2;
    sub k1 r1    by k1r1;

    sub k2 u1    by k2u1;
    sub k2 u2    by k2u2;

    sub k3 u1    by k3u1;
    sub k3 u2    by k3u2;
    sub k3 r1    by k3r1;

    sub k4 u1    by k4u1;
    sub k4 u2    by k4u2;
    sub k4 r1    by k4r1;

    sub ng u1   by ngu1;
    sub ng u2   by ngu2;

    sub ch1 u1    by ch1u1;
    sub ch1 u2    by ch1u2;

    sub ch2 u1    by ch2u1;
    sub ch2 u2    by ch2u2;
    sub ch2 r1    by ch2r1;

    sub ch3 u1    by ch3u1;
    sub ch3 u2    by ch3u2;
    sub ch3 r1    by ch3r1;

    sub ch4 u1    by ch4u1;
    sub ch4 u2    by ch4u2;
    sub ch4 r1    by ch4r1;

    sub nj u1    by nju1;
    sub nj u2    by nju2;

    sub t1 u1    by t1u1;
    sub t1 u2    by t1u2;

    sub t2 u1    by t2u1;
    sub t2 u2    by t2u2;

    sub t3 u1    by t3u1;
    sub t3 u2    by t3u2;

    sub t4 u1    by t4u1;
    sub t4 u2    by t4u2;

    sub nh u1    by nhu1;
    sub nh u2    by nhu2;

    sub th1 u1    by th1u1;
    sub th1 u2    by th1u2;
    sub th1 r1    by th1r1;

    sub th2 u1    by th2u1;
    sub th2 u2    by th2u2;

    sub th3 u1    by th3u1;
    sub th3 u2    by th3u2;
    sub th3 r1    by th3r1;

    sub th4 u1    by th4u1;
    sub th4 u2    by th4u2;
    sub th4 r1    by th4r1;

    sub n1 u1    by n1u1;
    sub n1 u2    by n1u2;
    sub n1 r1    by n1r1;

    sub p1 u1    by p1u1;
    sub p1 u2    by p1u2;
    sub p1 r1    by p1r1;

    sub p2 u1    by p2u1;
    sub p2 u2    by p2u2;

    sub p3 u1    by p3u1;
    sub p3 u2    by p3u2;
    sub p3 r1    by p3r1;

    sub p4 u1    by p4u1;
    sub p4 u2    by p4u2;
    sub p4 r1    by p4r1;

    sub m1 u1    by m1u1;
    sub m1 u2    by m1u2;
    sub m1 r1    by m1r1;

    sub xx y1 u1     by y2u1;
    sub xx y1 u2     by y2u2;
    sub v1 u1    by v1u1;
    sub v1 u2    by v1u2;
#    sub v1 r1    by v1r1;

#    sub z1 u1    by z1u1;
#    sub z1 u2    by z1u2;
#    sub z1 r1    by z1r1;

    sub sh u1    by shu1;
    sub sh u2    by shu2;

    sub s1 u1    by s1u1;
    sub s1 u2    by s1u2;
#    sub s1 r1    by s1r1;

    sub h1 u1    by h1u1;
    sub h1 u2    by h1u2;
    sub h1 r1    by h1r1;

    sub lh u1    by lhu1;
    sub lh u2    by lhu2;

    sub zh u1    by zhu1;
    sub zh u2    by zhu2;

    sub rh u1    by rhu1;
    sub rh u2    by rhu2;

} akhn_signs;

lookup akhn_nosigns_1 {
    sub k1 xx sh xx nh      by k1shnh;
    sub k1 xx sh xx m1      by k1shm1;
    sub k1 xx rh xx rh      by k1rhrh;
    sub k3 xx th3 xx th4      by k3th3th4;
    sub th1 xx s1 xx n1      by th1s1n1;
    sub p1 xx rh xx rh by p1rhrh;
    sub p2 xx rh xx rh by p2rhrh;
    sub y1 xx k1 xx k1 by y1k1k1;
    sub l3 xx k1 xx k1      by l3k1k1;
    sub sh xx k1 xx k1 by shk1k1;
    sub s1 xx k1 xx k1    by s1k1k1;
    sub s1 xx rh xx rh    by s1rhrh;
} akhn_nosigns_1;

lookup akhn_nosigns_2{
    sub k1 xx k1    by k1k1;
    sub k3 xx k3    by k3k3;
    sub ch1 xx ch1    by ch1ch1;
    sub ch3 xx ch3  by ch3ch3;
    sub t1 xx t1    by t1t1;
    sub t3 xx t3    by t3t3;
    sub th1 xx th1    by th1th1;
    sub th3 xx th3    by th3th3;
    sub n1 xx n1    by n1n1;
    sub p1 xx p1    by p1p1;
    sub p3 xx p3    by p3p3;
    sub m1 xx m1    by m1m1;
    sub y1 xx y1    by y1y1;
    sub v1 xx v1    by v1v1;
    sub z1 xx z1    by z1z1;
    sub s1 xx s1    by s1s1;
} akhn_nosigns_2;

lookup akhn_nosigns_3{
    sub k1 xx t1    by k1t1;
    sub k1 xx th1    by k1th1;
    sub k1 xx n1    by k1n1;
    sub k1 xx sh    by k1sh;
    sub k1 xx s1    by k1s1;
    sub k3 xx th3    by k3th3;
    sub k3 xx n1    by k3n1;
    sub k3 xx m1    by k3m1;
    sub k4 xx n1    by k4n1;
    sub ng xx k1    by ngk1;
    sub ng xx ng    by ngng;
    sub ch1 xx ch2    by ch1ch2;
    sub ch3 xx nj    by ch3nj;
    sub nj xx ch1    by njch1;
    sub nj xx ch2    by njch2;
    sub nj xx ch3    by njch3;
    sub nj xx nj    by njnj;
    sub t3 xx t4    by t3t4;
    sub nh xx t1    by nht1;
    sub nh xx t2    by nht2;
    sub nh xx t3    by nht3;
    sub nh xx t4    by nht4;
    sub nh xx nh    by nhnh;
    sub nh xx m1    by nhm1;
    sub th1 xx th2    by th1th2;
    sub th1 xx n1    by th1n1;
    sub th1 xx m1    by th1m1;
    sub th1 xx p4    by th1p4;
    sub th1 xx s1    by th1s1;
    sub th3 xx th4    by th3th4;
    sub n1 xx th1    by n1th1;
    sub n1 xx th2    by n1th2;
    sub n1 xx th3    by n1th3;
    sub n1 xx th4    by n1th4;
    sub n1 xx m1    by n1m1;
    sub [n1 n1cil] xx rh    by n1rh;
    sub p1 xx t1    by p1t1;
    sub p1 xx th1    by p1th1;
    sub p1 xx n1    by p1n1;
    sub p1 xx p2    by p1p2;
    sub p1 xx s1    by p1s1;
    sub p2 xx k1    by p2k1;
    sub p2 xx t1    by p2t1;
    sub p2 xx th1   by p2th1;
    sub p2 xx n1    by p2n1;
    sub p2 xx s1    by p2s1;
    sub p3 xx ch3    by p3ch3;
    sub p3 xx th3    by p3th3;
    sub p3 xx th4    by p3th4;
    sub p3 xx n1    by p3n1;
    sub m1 xx n1    by m1n1;
    sub m1 xx p1    by m1p1;
} akhn_nosigns_3;

lookup akhn_nosigns_4 {
    sub y1 xx k1    by y1k1;
#    sub y1 xx th1    by y1th1;
    sub y1 xx p1    by y1p1;
#    sub y1 xx m1    by y1m1;

    sub y1 u1    by y1u1;
    sub y1 u2    by y1u2;

    sub l3 xx k1    by l3k1;
    sub l3 xx p1    by l3p1;
    sub l3 xx m1    by l3m1;
#    sub z1 xx ch1    by z1ch1;
#    sub z1 xx ch2    by z1ch2;
    sub z1 xx n1    by z1n1;
    sub z1 xx m1    by z1m1;
    sub sh xx k1    by shk1;
#    sub sh xx t1    by sht1;
#    sub sh xx t2    by sht2;
#    sub sh xx nh    by shnh;
    sub sh xx p1    by shp1;
#    sub sh xx p2    by shp2;
    sub sh xx m1    by shm1;
    sub s1 xx k1    by s1k1;
#    sub s1 xx k2    by s1k2;
#    sub s1 xx th1    by s1th1;
    sub s1 xx th2    by s1th2;
    sub s1 xx n1    by s1n1;
    sub s1 xx p1    by s1p1;
#    sub s1 xx p2    by s1p2;
    sub s1 xx m1    by s1m1;
    sub h1 xx n1    by h1n1;
    sub h1 xx m1    by h1m1;
    sub lh xx lh    by lhlh;
    sub zh xx k1    by zhk1;
#    sub zh xx ch1    by zhch1;
    sub zh xx m1    by zhm1;
    sub zh xx s1    by zhs1;
    sub rh xx rh    by rhrh;
} akhn_nosigns_4;

# Needed only due to Windows/Uniscribe shaper
lookup pstf_signs {
    sub xx y1       by y2;
    sub xx v1       by v2;
} pstf_signs;

# Feautures (ligatures) - basic shaping forms
#----------

feature akhn {
    script mlm2;
        lookup akhn_cil;         #Chillu letters
        lookup akhn_ra_signs;    #Reph with u1/u2 signs
        lookup akhn_la_signs;    #La with u1/u2 signs
        lookup akhn_ra_nosigns;  #Reph without u1/u2 signs
        lookup akhn_la_nosigns;  #La without u1/u2 signs
        lookup akhn_signs;       #Longest conjuncts without reph/la forms
        lookup akhn_nosigns_1;   #Conjuncts with 3 consonants sans 'u1/u2/r1' signs
        lookup akhn_nosigns_2;   #Conjuncts with double consonants
        lookup akhn_nosigns_3;   #Conjuncts with 2 consonants sans 'u1/u2/r1' signs
        lookup akhn_nosigns_4;   #Conjuncts with ya/ra/la/za/sa/ha/lha/zha/rha
} akhn;

feature pstf {
    script mlm2;
        lookup pstf_signs;
} pstf;
