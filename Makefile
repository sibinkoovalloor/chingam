FONT_DIR:=fonts
TEST_DIR=tests
BUILDSCRIPT:=tools/build.py
CONFFILE:=config
export FTYPE:=otf

ttf otf woff woff2: override FTYPE=$@
ttf otf woff woff2:
	mkdir -p $(FONT_DIR)/$(FTYPE)/
	python3 $(BUILDSCRIPT) $(CONFFILE) $@

pdf: 
	cd $(TEST_DIR) && xelatex human-rights.tex && \
		xdg-open human-rights.pdf

clean:
	rm -f $(FONT_DIR)/$(FTYPE)/*.{$(FTYPE),sfd}

#Usage: make beta
beta: VERSION=0.9.4
beta: release

release: clean otf ttf woff2

test: $(FONT_DIR)/otf/*.otf
	python3 $(TEST_DIR)/glyph_without_shaping.py $(FONT_DIR)/otf/*.otf
